# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: marvin <marvin@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2022/12/09 20:17:24 by marvin            #+#    #+#              #
#    Updated: 2022/12/09 20:17:24 by marvin           ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = push_swap

FLAGS = #-Wall -Wextra -Werror -I .

SRCS =	src/check_error.c \
		src/ft_join.c \
		src/push.c \
		src/reverse_rotate.c \
		src/rotate.c \
		src/swap.c \
		src/ft_atoi.c \
		src/utils.c \
		src/index_tab.c \
		src/ft_convert_base.c \
		src/ft_split.c \
		src/sort.c \
		main.c

OBJS = ${SRCS:.c=.o}

HEADER = includes/push_swap.h

all :	${NAME}

${NAME}:	${OBJS}
		${CC} ${FLAGS} ${SRCS} -o ${NAME}

%.o:	%.c ${HEADER} Makefile
		${CC} ${FLAGS} -c $< -o $@

clean:	
		${RM} ${OBJS}

fclean:	clean
		${RM} ${NAME}

re:	fclean
	${MAKE} all 

.PHONY: all clean fclean re