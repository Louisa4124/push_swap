/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 12:15:54 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/03 13:54:50 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include <unistd.h>
# include <stdlib.h>
# include <sys/types.h>
# include <sys/uio.h>
# include <limits.h>

/* Instructions */
void	ft_pa(int *a, int *b, int len);
void	ft_pb(int *b, int *a, int len);
void	ft_ra(int *tab, int len);
void	ft_rb(int *tab, int len);
void	ft_rr(int *a, int *b, int len);
void	ft_sa(int *a);
void	ft_sb(int *b);
void	ft_ss(int *a, int *b);
void	ft_rra(int *tab, int len);
void	ft_rrb(int *tab, int len);
void	ft_rrr(int *a, int *b, int len);

/* Check error functions */
int		ft_ischar_valid(char *a);
int		ft_isint_valid(char *tab);
int		check_error(int argc, char **tab);

/* Utils functions */
char	*ft_join(char *argv[], int argc);
int		ft_len_argv(char **argv);
int		ft_atoi(const char *str);
void	ft_free(char **a);
int		ft_getlen(char **a);
char	*ft_itoa(int n);
size_t	ft_strlen(const char *s);
char	*ft_strndup(char *src, int n);
char	**ft_split(char *str, char c);
size_t	ft_strlcpyps(char *dest, const char *src);
char	*ft_convert_base(char *nbr, char *base_from, char *base_to);

/* Index functions */
char	*ft_check_lower(char **tab, int i);
char	**index_tab(char **tab);
int		*index_tab_int(char **tab, int len);

/* Sort functions */
int		*stack_a(int *a, int len);
void	sort(int *a, int *b, int size);

#endif
