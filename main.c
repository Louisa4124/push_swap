/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/16 14:57:09 by lboudjem          #+#    #+#             */
/*   Updated: 2022/12/16 14:57:09 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "includes/push_swap.h"
#include <stdio.h>

// segfault si aucun argument
// 4 args
// 1 2 

int	*fill_b(int *b, int len)
{
	int	tmp;

	tmp = 0;
	b = malloc(len * sizeof(int));
	if (!b)
		return (NULL);
	while (tmp < len)
	{
		b[tmp] = -1;
		tmp++;
	}
	return (b);
}

int	main(int argc, char *argv[])
{
	char	*args;
	char	**values;
	char	**tmp;
	int		*index;
	int		*b;

	args = ft_join(argv, argc);
	if (!args)
		return (0);
	values = ft_split(args, ' ');
	if (!values)
		return (0);
	if (!check_error(ft_getlen(values) - 1, values))
	{
		write(2, "Error", 5);
		return (0);
	}
	tmp = index_tab(values);
	index = index_tab_int(tmp, ft_getlen(values));
	b = fill_b(b, ft_getlen(values));
	sort(index, b, ft_getlen(values));
	return (0);
}

/* leaks -atExit -- ./push_swap 1 5 2 1 */
