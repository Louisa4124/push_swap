/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   check_error.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 15:57:27 by lboudjem          #+#    #+#             */
/*   Updated: 2022/12/21 15:21:22 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	ft_ischar_valid(char *a)
{
	long int	c;
	int			i;
	int			sign;

	i = 0;
	c = 0;
	sign = 1;
	if (!a || !a[0])
		return (0);
	if (a[i] == '-')
	{
		sign = -1;
		i++;
	}
	while (a[i])
	{
		if (a[i] < '0' || a[i] > '9')
			return (0);
		c = c * 10 + (a[i] - '0');
		if (c * sign > INT_MAX || c * sign < INT_MIN)
			return (0);
		i++;
	}
	return (1);
}

int	ft_isint_valid(char *tab)
{
	int	i;
	int	nb;

	i = 0;
	nb = 0;
	if (tab[i] == '-')
		i++;
	while (tab[i])
	{
		if (!(tab[i] >= '0' && tab[i] <= '9'))
			return (0);
		else
			nb = 1;
		i++;
	}
	if (nb == 0)
		return (0);
	return (1);
}

int	check_error(int argc, char **tab)
{
	int	i;
	int	j;

	i = 0;
	while (i <= argc)
	{
		if (!ft_isint_valid(tab[i]) || !ft_ischar_valid(tab[i]))
		{
			ft_free(tab);
			return (0);
		}
		j = 0;
		while (j < argc && ft_atoi(tab[i]) != ft_atoi(tab[j]))
			j++;
		if (ft_atoi(tab[i]) == ft_atoi(tab[j]) && i != j)
		{
			ft_free(tab);
			return (0);
		}
		i++;
	}
	return (1);
}
