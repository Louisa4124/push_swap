/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_join.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/16 14:30:16 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/03 15:18:00 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	ft_len_argv(char **argv)
{
	int	i;
	int	j;
	int	len;

	i = 1;
	len = 0;
	while (argv[i])
	{
		j = 0;
		while (argv[i][j])
		{
			j++;
			len++;
		}
		i++;
	}
	return (len);
}

char	*ft_join(char *argv[], int argc)
{
	int		i;
	int		j;
	int		len;
	char	*str;

	str = malloc((ft_len_argv(argv) + argc));
	if (!str)
		return (0);
	i = 1;
	len = -1;
	while (argv[i])
	{
		j = -1;
		while (argv[i][++j])
			str[++len] = argv[i][j];
		str[++len] = ' ';
		i++;
	}
	str[len + 1] = '\0';
	return (str);
}
