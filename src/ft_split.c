/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/21 15:14:03 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/03 13:52:23 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

static void	ft_freesplit(char **split)
{
	size_t	tmp;

	tmp = 0;
	while (split[tmp])
	{
		free(split[tmp]);
		tmp++;
	}
	free(split);
}

static int	ft_get_size(char *str, char c)
{
	int	i;

	i = 0;
	while (str && str[i] && (str[i] != c))
		i++;
	return (i);
}

static int	ft_wordcount(char *str, char c)
{
	int	w;

	w = 0;
	if (!str)
		return (0);
	while (*str)
	{
		while (*str == c)
			str++;
		if (*str)
			w++;
		while (*str != c && *str)
			str++;
	}
	return (w);
}

char	**ft_split(char *str, char c)
{
	char	**t;
	int		size;
	int		i;

	size = ft_wordcount(str, c);
	t = (char **)malloc((size + 1) * sizeof(char *));
	if (t == NULL)
		return (NULL);
	i = 0;
	while (str && *str && i < size)
	{
		while (*str == c)
			str++;
		t[i] = ft_strndup(str, ft_get_size(str, c));
		if (!t[i])
		{
			ft_freesplit(t);
			return (NULL);
		}
		str += ft_get_size(str, c);
		i++;
	}
	t[size] = 0;
	return (t);
}
