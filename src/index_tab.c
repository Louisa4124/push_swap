/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   index_tab.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/17 16:12:23 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/05 15:29:09 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

char	*ft_check_lower(char **tab, int i)
{
	int	j;
	int	index;

	j = 0;
	index = 0;
	while (j < ft_getlen(tab))
	{
		if (ft_atoi(tab[j]) < ft_atoi(tab[i]))
			index++;
		j++;
	}
	return (ft_itoa(index));
}

char	**index_tab(char **tab)
{
	int		len;
	char	**tmp;
	char	*index;
	int		i;

	i = 0;
	len = ft_getlen(tab);
	tmp = malloc((len) * sizeof(char *));
	if (!tmp)
		return (0);
	while (i < len)
	{
		index = ft_check_lower(tab, i);
		tmp[i] = ft_strndup(index, ft_strlen(index));
		ft_strlcpyps(tmp[i], index);
		i++;
	}
	return (tmp);
}

int	*index_tab_int(char **tab, int len)
{
	int	*res;
	int	tmp;

	res = malloc(len * sizeof(int));
	if (!res)
		return (NULL);
	tmp = 0;
	while (tmp < len)
	{
		res[tmp] = ft_atoi(tab[tmp]) + 1;
		tmp++;
	}
	return (res);
}
