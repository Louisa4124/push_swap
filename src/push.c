/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 13:50:17 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/03 13:23:49 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_pa(int *a, int *b, int len)
{
	int	i;

	if (!b[0])
		return ;
	i = len - 1;
	while (--i >= 0)
		a[i + 1] = a[i];
	a[0] = b[0];
	while (++i < len - 1)
		b[i] = b[i + 1];
	b[i] = -1;
	write(1, "pa\n", 3);
}

void	ft_pb(int *b, int *a, int len)
{
	int	i;

	if (!a[0])
		return ;
	i = len - 1;
	while (--i >= 0)
		b[i + 1] = b[i];
	b[0] = a[0];
	while (++i < len - 1)
		a[i] = a[i + 1];
	a[i] = -1;
	write(1, "pb\n", 3);
}
