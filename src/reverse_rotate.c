/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reverse_rotate.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 14:49:57 by lboudjem          #+#    #+#             */
/*   Updated: 2022/12/16 15:02:35 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_rra(int *tab, int len)
{
	int	tmp;
	int	i;

	i = len - 1;
	tmp = tab[i];
	while (i >= 0)
	{
		i--;
		tab[i + 1] = tab[i];
	}
	tab[0] = tmp;
	write(1, "rra\n", 4);
}

void	ft_rrb(int *tab, int len)
{
	int	tmp;
	int	i;

	i = len - 1;
	tmp = tab[i];
	while (i >= 0)
	{
		i--;
		tab[i + 1] = tab[i];
	}
	tab[0] = tmp;
	write(1, "rrb\n", 4);
}

void	ft_rrr(int *a, int *b, int len)
{
	ft_rra(a, len);
	ft_rrb(b, len);
}
