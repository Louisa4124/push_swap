/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotate.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/15 14:12:14 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/03 13:23:55 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_ra(int *tab, int len)
{
	int	tmp;
	int	i;

	i = 0;
	tmp = tab[0];
	while (i < len)
	{
		tab[i] = tab[i + 1];
		i++;
	}
	tab[len - 1] = tmp;
	write(1, "ra\n", 3);
}

void	ft_rb(int *tab, int len)
{
	int	tmp;
	int	i;

	i = 0;
	tmp = tab[0];
	while (i < len)
	{
		tab[i] = tab[i + 1];
		i++;
	}
	tab[len - 1] = tmp;
	write(1, "rb\n", 3);
}

void	ft_rr(int *a, int *b, int len)
{
	ft_ra(a, len);
	ft_rb(b, len);
}
