/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   smol_sort.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2023/01/06 12:17:36 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/09 17:20:57 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	sort_three(int *a, int *b)
{
	if (a[0] > a[1] && a[1] < a[2] && a[0] < a[2])
		sa(a);
	else if (a[0] > a[1] && a[1] < a[2] && a[0] > a[2])
		ra(a, 3);
	else if (a[0] > a[1] && a[1] > a[2] && a[0] > a[2])
	{
		sa(a);
		rra(a, 3);
	}
	else if (a[0] < a[1] && a[1] > a[2] && a[0] < a[2])
	{
		sa(a);
		ra(a, 3);
	}
	else if (a[0] < a[1] && a[1] > a[2] && a[0] > a[2])
		rra(a, 3);
}

void	sort_five(int *a, int *b)
{
	pb(b, a, 5);
	pb(b, a, 5);
	sort_three(a, b);
	pa(a, b, 5);
	if (a[0] > a [3])
		ra(a, 3);
	else if (a[0] > a[1] && a[0] < a[2])
		sa(a);
	else if (a[0] > a[1] && a[0] > a[2] && a[0] < a[3])
	{
		rra(a, 4);
		sa(a);
		ra(a, 4);
		ra(a, 4);
	}
	pa(a, b, 5);
	if (a[0] > a[4])
		
}
