/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sort.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/21 15:35:40 by lboudjem          #+#    #+#             */
/*   Updated: 2023/01/06 12:15:34 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

int	check_if_sorted(int *a, int len)
{
	int	tmp;

	tmp = 0;
	while (tmp < len - 1)
	{
		if (a[tmp + 1] < a[tmp])
			return (1);
		tmp++;
	}
	return (0);
}

int	*stack_a(int *a, int len)
{
	int	tmp;
	int	tmp2;
	int	*a2;

	tmp = 0;
	tmp2 = 0;
	a2 = malloc(len * sizeof(int));
	if (!a2)
		return (NULL);
	while (tmp < len)
	{
		if (a[tmp] != -1)
		{
			a2[tmp2] = a[tmp];
			tmp2++;
		}
		tmp++;
	}
	while (tmp2 < tmp)
	{
		a2[tmp2] = -1;
		tmp2++;
	}
	return (a2);
}

int	max_bits(int size)
{
	int	max_num;
	int	max_bits;

	max_bits = 0;
	max_num = size - 1;
	while ((max_num >> max_bits) != 0)
		++max_bits;
	return (max_bits);
}

void	sort(int *a, int *b, int size)
{
	int	i;
	int	j;
	int	num;

	i = 0;
	while (i < max_bits(size))
	{
		j = 0;
		while (j < size && check_if_sorted(a, size) == 1)
		{
			num = a[0];
			if (((num >> i) & 1) == 1)
			{
				ft_ra(a, size);
				a = stack_a(a, size);
			}
			else
			{
				ft_pb(b, a, size);
				a = stack_a(a, size);
			}
			j++;
		}
		i++;
		while (b[0] != -1)
			ft_pa(a, b, size);
		a = stack_a(a, size);
	}
}
