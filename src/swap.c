/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swap.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lboudjem <lboudjem@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2022/12/10 12:12:25 by lboudjem          #+#    #+#             */
/*   Updated: 2022/12/10 12:12:25 by lboudjem         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "../includes/push_swap.h"

void	ft_sa(int *a)
{
	int	tmp;

	if (a[0] && a[1])
	{
		tmp = a[0];
		a[0] = a[1];
		a[1] = tmp;
	}
	write(1, "sa\n", 3);
}

void	ft_sb(int *b)
{
	int	tmp;

	if (b[0] && b[1])
	{
		tmp = b[0];
		b[0] = b[1];
		b[1] = tmp;
	}
	write(1, "sb\n", 3);
}

void	ft_ss(int *a, int *b)
{
	ft_sa(a);
	ft_sb(b);
}
